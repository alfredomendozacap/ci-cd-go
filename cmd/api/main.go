package main

import (
	"log"

	"gitlab.com/alfredomendozacap/ci-cd-go/cmd/api/bootstrap"
)

func main() {
	if err := bootstrap.Run(); err != nil {
		log.Fatal(err)
	}
}
