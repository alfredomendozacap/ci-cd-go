package bootstrap

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"

	// mysql driver
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/alfredomendozacap/ci-cd-go/internal/platform/server"
	"gitlab.com/alfredomendozacap/ci-cd-go/internal/platform/storage/mysql"
)

var (
	port = 8080
)

const (
	dbUser = "root"
	dbPass = ""
	dbHost = "localhost"
	dbPort = "3306"
	dbName = "ci_cd"
)

// Run server
func Run() error {
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?parseTime=true&tls=false&autocommit=true&charset=utf8mb4,utf8",
		dbUser, dbPass, dbHost, dbPort, dbName,
	)

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return err
	}

	newTaskRepository := mysql.NewTaskRepository(db)

	if os.Getenv("PORT") != "" {
		port, _ = strconv.Atoi(os.Getenv("PORT"))
	}

	srv := server.New(uint(port), newTaskRepository)
	return srv.Run()
}
