package health

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
)

// CheckHandler returns an HTTP handler to perform health checks.
func CheckHandler() fiber.Handler {
	return func(c *fiber.Ctx) error {
		return c.Status(http.StatusOK).SendString("everything is ok!")
	}
}
