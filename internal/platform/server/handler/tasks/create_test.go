package tasks

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/alfredomendozacap/ci-cd-go/internal/platform/storage/mocks"
)

func TestHandler_Create(t *testing.T) {
	taskRepository := new(mocks.TaskRepository)

	app := fiber.New()
	app.Post("/tasks", CreateHandler(taskRepository))

	t.Run("given an invalid request it returns 400", func(t *testing.T) {
		createCourseReq := createRequest{
			Title: "Demo Title",
		}

		b, _ := json.Marshal(createCourseReq)

		req := httptest.NewRequest(http.MethodPost, "/tasks", bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")

		res, _ := app.Test(req)
		defer res.Body.Close()

		if http.StatusUnprocessableEntity != res.StatusCode {
			t.Errorf("want %v, got %v status code", http.StatusUnprocessableEntity, res.StatusCode)
		}
	})

	t.Run("given a valid request it returns 201", func(t *testing.T) {
		createCourseReq := createRequest{
			ID:    "8a1c5cdc-ba57-445a-994d-aa412d23723f",
			Title: "Demo Title",
		}

		b, _ := json.Marshal(createCourseReq)

		req := httptest.NewRequest(http.MethodPost, "/tasks", bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")

		res, _ := app.Test(req)
		defer res.Body.Close()

		if http.StatusCreated != res.StatusCode {
			t.Errorf("want %v, got %v status code", http.StatusCreated, res.StatusCode)
		}
	})
}
