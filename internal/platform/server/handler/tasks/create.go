package tasks

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	public "gitlab.com/alfredomendozacap/ci-cd-go/internal"
)

type createRequest struct {
	ID    string `json:"id" validate:"required"`
	Title string `json:"title" validate:"required"`
}

type ErrorResponse struct {
	FailedField string `json:"field"`
	Tag         string `json:"tag"`
	Value       string `json:"value"`
}

func ValidateStruct(createRequest *createRequest) []*ErrorResponse {
	var errors []*ErrorResponse
	validate := validator.New()
	err := validate.Struct(createRequest)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ErrorResponse
			element.FailedField = err.Field()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}
	return errors
}

// CreateHandler returns an HTTP handler for tasks creation.
func CreateHandler(taskRepository public.TaskRepository) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		var req createRequest
		if err := ctx.BodyParser(&req); err != nil {
			return ctx.Status(http.StatusBadRequest).JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		errors := ValidateStruct(&req)
		if errors != nil {
			return ctx.Status(http.StatusUnprocessableEntity).JSON(errors)
		}

		task := public.NewTask(req.ID, req.Title, false)
		if err := taskRepository.Save(ctx.Context(), task); err != nil {
			return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"message": err.Error(),
			})
		}

		return ctx.SendStatus(http.StatusCreated)
	}
}
