package server

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	public "gitlab.com/alfredomendozacap/ci-cd-go/internal"
	"gitlab.com/alfredomendozacap/ci-cd-go/internal/platform/server/handler/health"
	"gitlab.com/alfredomendozacap/ci-cd-go/internal/platform/server/handler/tasks"
)

// Server model
type Server struct {
	httpAddr       string
	engine         *fiber.App
	taskRepository public.TaskRepository
}

// New Server
func New(port uint, taskRepository public.TaskRepository) Server {
	srv := Server{
		engine:   fiber.New(),
		httpAddr: fmt.Sprintf(":%d", port),

		taskRepository: taskRepository,
	}

	srv.registerRoutes()
	return srv
}

// Run server
func (s *Server) Run() error {
	log.Println("Server running on", s.httpAddr)
	return s.engine.Listen(s.httpAddr)
}

func (s *Server) registerRoutes() {
	s.engine.Get("/health", health.CheckHandler())
	s.engine.Post("/tasks", tasks.CreateHandler(s.taskRepository))
}
