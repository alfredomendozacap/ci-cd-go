package mysql

const (
	sqlTaskTable = "tasks"
)

type sqlTask struct {
	ID     string `db:"id"`
	Title  string `db:"title"`
	IsDone bool   `db:"is_done"`
}
