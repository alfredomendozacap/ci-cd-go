package mysql

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/huandu/go-sqlbuilder"
	public "gitlab.com/alfredomendozacap/ci-cd-go/internal"
)

// TaskRepository is a MySQL mooc.TaskRepository implementation.
type TaskRepository struct {
	db *sql.DB
}

// NewTaskRepository initializes a MySQL-based implementation of mooc.TaskRepository.
func NewTaskRepository(db *sql.DB) *TaskRepository {
	return &TaskRepository{
		db: db,
	}
}

// Save implements the mooc.TaskRepository interface.
func (r *TaskRepository) Save(ctx context.Context, task *public.Task) error {
	taskSQLStruct := sqlbuilder.NewStruct(new(sqlTask))
	query, args := taskSQLStruct.InsertInto(sqlTaskTable, &sqlTask{
		ID:     task.ID(),
		Title:  task.Title(),
		IsDone: task.IsDone(),
	}).Build()

	_, err := r.db.ExecContext(ctx, query, args...)
	if err != nil {
		return fmt.Errorf("error trying to persist course on database: %v", err)
	}

	return nil
}
