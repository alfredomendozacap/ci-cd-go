package public

import "context"

// Task is the data structure that represents a Task.
type Task struct {
	id     string
	title  string
	isDone bool
}

// NewTask creates a new Task.
func NewTask(id, title string, isDone bool) *Task {
	return &Task{
		id:     id,
		title:  title,
		isDone: isDone,
	}
}

// TaskRepository defines the expected behaviour from a task storage.
type TaskRepository interface {
	Save(ctx context.Context, task *Task) error
}

// ID returns the Task unique identifier.
func (t *Task) ID() string {
	return t.id
}

// Title returns the Task Title.
func (t *Task) Title() string {
	return t.title
}

// IsDone returns the Task isDone.
func (t *Task) IsDone() bool {
	return t.isDone
}
