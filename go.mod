module gitlab.com/alfredomendozacap/ci-cd-go

go 1.16

require (
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gofiber/fiber/v2 v2.5.0
	github.com/huandu/go-sqlbuilder v1.12.0 // indirect
	github.com/klauspost/compress v1.11.9 // indirect
	github.com/valyala/fasthttp v1.22.0 // indirect
	golang.org/x/sys v0.0.0-20210301091718-77cc2087c03b // indirect
)
